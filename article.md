title: "Podrobná mapa: ANO bralo všude, Zelení dostali výprask i od vlastních"
perex: "Srovnání s volbami do sněmovny v roce 2013 ukazuje, že vítězné ANO sbíralo nové voliče prakticky všude a na úkor ostatních stran. Okamura posílil hlavně v pohraničí a Zelení dostali výprask i v tradičních baštách."
authors: ["Jan Cibulka"]
published: "22. října 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/volebni_debata_danel_171020-170936_rez_1.jpg
coverimg_note: "Foto Michaela Danělová, ČRo"
styles: ["https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.4.2/ol.css"]
libraries: ["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js", "https://ft-polyfill-service.herokuapp.com/v2/polyfill.min.js?features=Object.values,String.prototype.startsWith", "https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.4.2/ol.js"]
options: "" #wide
---

Obce na jihovýchod od Prahy jsou jediná souvislá oblast, kde vítěz voleb, hnutí ANO 2011, ztratil oproti minulým volbám podporu, například přímo v Jesenici šla podpora partaje dolů o necelé tři [procentní body](http://www.statistikaamy.cz/2016/02/procentni-bod-a-procento/). Andrej Babiš tak nabíral nové příznivce i v hlavním městě, ve srovnání se zbytkem republiky jich ale bylo méně.

_Výsledky voleb v roce 2013 a nyní můžete prozkoumat v následující mapě. <span style="color: #4575b4;">Modré odstíny</span> značí zisk, <span style="color: #d73027;">červenooranžové</span> pak ztrátu._

_V mapě jsou všechny letos kandidující strany, pokud v minulých volbách nekandidovaly (např. kandidáti STAN byli na kandidátkách lidovců), mapa ukazuje jen jejich letošní výsledek. U Okamurovy SPD pak bereme jako její předchůdkyni Úsvit přímé demokracie._

<wide>
<div id="mapdiv">
	<div id="select"></div>
	<div id="tooltip">Najetím vyberte obec.</div>
	<div id="map" class="map"></div>
	<div id="legend">
		<div id="scale"></div>
		<span class="zisk">zisk</span>
		<span class="ztrata">ztráta</span>
	</div>
	 <form action="?" id='frm-geocode'>
	  <label for="inp-geocode">Najít adresu</label>
	  <div class="inputs">
	    <input type="text" id="inp-geocode" placeholder="Bruntál">
	    <input type="submit" value="Najít">
	  </div>
	</form>
</div>
</wide>

V metropoli a okolí pak jednoznačně získala Pirátská strana, nárůsty v jednotlivých obcích i městských částech byly často přes 10 procentních bodů. Ve stejných místech naopak ztrácela Strana zelených, markantní to bylo zejména v tradiční baště této strany, na Praze 6. Strana ale ztrácela prakticky v celé republice, výrazně pak například v Brně.

„Zeleným se již několikrát po sobě nepodařilo dostat do parlamentu, tj. jejich voličům došla trpělivost a hlasují pro zelené pouze v rámci komunální politiky, nikoli v rámci voleb do Parlamentu,“ vysvětluje sociální geograf z Univerzity Karlovy [Martin Šimon](http://www.soc.cas.cz/lide/martin-simon).

Za zmínku stojí i propad komunistů v Severních Čechách a v pohraničí, v této oblasti posilovalo jak zmíněné ANO, tak i SPD Tomia Okamury. A právě Okamura doslova vymazal Dělnickou stranu sociální spravedlnosti. „Podobně to bylo i dříve, kdy se radikálně-pravicové názory internalizovali do politiky větších stran,“ dodává Šimon.